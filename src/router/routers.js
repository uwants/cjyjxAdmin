/* eslint-disable */
import Main from '@/components/main'
import parentView from '@/components/parent-view'

/**
 * iview-admin中meta除了原生参数外可配置的参数:
 * meta: {
 *  title: { String|Number|Function }
 *         显示在侧边栏、面包屑和标签栏的文字
 *         使用'{{ 多语言字段 }}'形式结合多语言使用，例子看多语言的路由配置;
 *         可以传入一个回调函数，参数是当前路由对象，例子看动态路由和带参路由
 *  hideInBread: (false) 设为true后此级路由将不会出现在面包屑中，示例看QQ群路由配置
 *  hideInMenu: (false) 设为true后在左侧菜单不会显示该页面选项
 *  notCache: (false) 设为true后页面在切换标签后不会缓存，如果需要缓存，无需设置这个字段，而且需要设置页面组件name属性和路由配置的name一致
 *  access: (null) 可访问该页面的权限数组，当前路由设置的权限会影响子路由
 *  icon: (-) 该页面在左侧菜单、面包屑和标签导航处显示的图标，如果是自定义图标，需要在图标名称前加下划线'_'
 *  beforeCloseName: (-) 设置该字段，则在关闭当前tab页时会去'@/router/before-close.js'里寻找该字段名对应的方法，作为关闭前的钩子函数
 * }
 */


export default [
	{
		path: '/',
		name: 'login',
		meta: {
			title: 'Login - 登录',
			hideInMenu: true
		},
		component: () => import('@/view/login/login.vue')
	},


	{
		path: '/home',
		name: 'home',
		redirect: '/',
		component: Main,
		meta: {
			hideInMenu: true,
			notCache: true
		},
		children: [
			{
				path: '/home',
				name: 'home',
				meta: {
					hideInMenu: true,
					title: '首页',
					notCache: true,
					icon: 'md-home'
				},
				component: () => import('@/view/single-page/home/home_major.vue')
			}
		]
	},


	//


	//页面路由


	{
		path: '/message',
		name: 'message',
		component: Main,
		meta: {
			hideInBread: true,
			hideInMenu: true
		},
		children: [
			{
				path: 'message_page',
				name: '消息中心',
				meta: {
					icon: 'md-notifications',
					title: '消息中心'
				},
				component: () => import('@/view/single-page/message/index.vue')
			},
			{
				path: 'adminMessage_page',
				name: '个人资料',
				meta: {
					icon: 'md-flower',
					title: '个人资料',
				},
				component: () => import('@/view/single-page/admin/index.vue')
			}
		]
	},


	{
		path: '/401',
		name: 'error_401',
		meta: {
			hideInMenu: true
		},
		component: () => import('@/view/error-page/401.vue')
	},
	{
		path: '/500',
		name: 'error_500',
		meta: {
			hideInMenu: true
		},
		component: () => import('@/view/error-page/500.vue')
	},
	{
		path: '*',
		name: 'error_404',
		meta: {
			hideInMenu: true
		},
		component: () => import('@/view/error-page/404.vue')
	},


	{
		path: '/member',
		name: '用户中心',
		meta: {
			icon: 'md-person',
			title: '用户中心'
		},
		component: Main,
		children: [
			{
				path: '/memberList',
				name: 'memberList',
				meta: {
					icon: '',
					title: '用户列表'
				},
				component: () => import('@/view/member/member.vue'),
			},
			{
				path: 'queryMember',
				name: 'queryMember',
				meta: {
					hideInMenu: true,
					icon: 'md-flower',
					title: route => `用户详情 - ${route.query.id}`,
					notCache: true
				},
				component: () => import('@/view/member/member_message.vue')
				//component: () => import('@/view/member/test.vue')
			},


		]
	},


	{
		path: '/company',
		name: '企业中心',
		component: Main,
		meta: {
			icon: 'md-home',
			title: '企业中心'
		},
		children: [
			{
				path: '/companyList',
				name: 'companyList',
				meta: {
					icon: '',
					title: '企业列表'
				},
				component: () => import('@/view/company/company_list.vue')
			},
			{
				path: 'queryCompany',
				name: 'queryCompany',
				meta: {
					hideInMenu: true,
					icon: 'md-flower',
					title: route => `企业详情 - ${route.query.id}`,
					notCache: true
				},
				component: () => import('@/view/company/company_message.vue')
				//component: () => import('@/view/member/test.vue')
			},
			{
				path: 'editCompany',
				name: 'editCompany',
				meta: {
					hideInMenu: true,
					icon: 'md-flower',
					title: route => `企业编辑 - ${route.query.id}`,
					notCache: true
				},
				component: () => import('@/view/company/company_edit.vue')
				//component: () => import('@/view/member/test.vue')
			},
			{
				path: '/companyExamine',
				name: 'companyExamine',
				meta: {
					icon: '',
					title: '认证审核'
				},
				component: () => import('@/view/companyExamine/list.vue')
			},
			{
				path: 'examineCompany',
				name: 'examineCompany',
				meta: {
					hideInMenu: true,
					icon: 'md-flower',
					title: route => `认领审核 - ${route.query.id}`,
					notCache: true
				},
				component: () => import('@/view/companyExamine/message.vue')
			},
			{

				path: '/companyHot',
				name: 'companyHot',
				meta: {
					icon: '',
					title: '热门企业'
				},
				component: () => import('@/view/companyHot/list.vue')
			},
		]
	},
	{
		path: '/company',
		name: '意见中心',
		component: Main,
		meta: {
			icon: 'ios-albums',
			title: '意见中心'
		},
		children: [
			{
				path: '/commentList',
				name: 'commentList',
				meta: {
					icon: '',
					title: '意见列表'
				},
				component: () => import('@/view/comment/comment_list.vue')
			},
			{
				path: 'queryComment',
				name: 'queryComment',
				meta: {
					hideInMenu: true,
					icon: 'md-flower',
					title: route => `${route.query.name} - ${route.query.id}`,
					notCache: true
				},
				component: () => import('@/view/comment/comment_message.vue')
			},
			{
				path: '/repliesList',
				name: 'replies_list',
				meta: {
					icon: '',
					title: '意见回复'
				},
				component: () => import('@/view/replies/replies_list')
			},
			{
				path: 'queryReply',
				name: 'queryReply',
				meta: {
					hideInMenu: true,
					icon: 'md-flower',
					title: route => `${route.query.name} - ${route.query.id}`,
					notCache: true
				},
				component: () => import('@/view/replies/replies_message')
			},
			{
				path: '/solicitationList',
				name: 'solicitation_list',
				meta: {
					icon: '',
					title: '意见箱列表'
				},
				component: () => import('@/view/solicitation/solicitation_list.vue')
			},
			{
				path: 'querySolicitation',
				name: 'querySolicitation',
				meta: {
					hideInMenu: true,
					icon: 'md-flower',
					title: route => `${route.query.name} - ${route.query.id}`,
					notCache: true
				},
				component: () => import('@/view/solicitation/solicitation_message.vue')
			},
			{
				path: 'querySolicitationList',
				name: 'querySolicitationList',
				meta: {
					hideInMenu: true,
					icon: 'md-flower',
					title: route => `${route.query.name} - ${route.query.id}`,
					notCache: true
				},
				component: () => import('@/view/solicitation/solicitation_comment_list.vue')
			},
		]
	},

	{
		path: '/service',
		name: '服务中心',
		component: Main,
		meta: {
			icon: 'logo-freebsd-devil',
			title: '服务中心'
		},
		children: [
			/*{
			  path: '/weixinTemplate',
			  name: '微信消息模板',
			  meta: {
				icon: '',
				title: '微信消息模板'
			  },
			  component: () => import('@/view/weixinTemplate/list.vue')
			},
			/*{
			  path: '/messageTemplate',
			  name: 'queryTemplate',
			  meta: {
				hideInMenu: true,
				icon: 'md-flower',
				title: route => `模板消息 - ${route.query.name}`,
				notCache: true
			  },
			  component: () => import('@/view/weixinTemplate/send_message.vue')
			},*/
			{
				path: '/customMenu',
				name: 'customMenu',
				meta: {
					icon: '',
					title: '自定义菜单'
				},
				component: () => import('@/view/customMenu/message.vue')
			},
		]
	},
	/*{
		  path: '/data',
		  name: '数据中心',
		  component: Main,
		  meta: {
			  icon: 'md-stats',
			  title: '数据中心'
		  },
		  children: [
			  {
				  path: '/userData',
				  name: '用户数据',
				  meta: {
					  icon: '',
					  title: '用户数据'
				  },
				  component: () => import('@/view/data/user-data.vue')
			  },
			  {
				  path: '/everyData',
				  name: '每日数据',
				  meta: {
					  icon: '',
					  title: '每日数据'
				  },
				  component: () => import('@/view/data/every-data.vue')
			  },
			  {
				  path: '/activityData',
				  name: '活动数据',
				  meta: {
					  icon: '',
					  title: '活动数据'
				  },
				  component: () => import('@/view/data/data_center.vue')
			  }
	  /*
	{
	  path: '/data',
	  name: '数据中心',
	  component: Main,
	  meta: {
		icon: 'md-stats',
		title: '数据中心'
	  },
	  children: [
		{
		  path: '/userData',
		  name: '用户数据',
		  meta: {
			icon: '',
			title: '用户数据'
		  },
		  component: () => import('@/view/data/user-data.vue')
		},
		{
		  path: '/everyData',
		  name: '每日数据',
		  meta: {
			icon: '',
			title: '每日数据'
		  },
		  component: () => import('@/view/data/every-data.vue')
		},
		{
		  path: '/activityData',
		  name: '活动数据',
		  meta: {
			icon: '',
			title: '活动数据'
		  },
		  component: () => import('@/view/data/data_center.vue')
		}

	  ]
	},*/

	{
		path: '/applets',
		name: 'applets',
		component: Main,
		meta: {
			icon: 'ios-appstore',
			title: '小程序'
		},
		children: [
			{
				path: '/appletSkin',
				name: 'appletSkin',
				meta: {
					icon: '',
					title: '小程序皮肤管理'
				},
				component: () => import('@/view/appletSkin/message.vue')
			},
			/*
			{
				path: '/applatsEdition',
				name: '小程序版本管理',
			meta: {
			  icon: '',
			  title: '小程序版本管理'
			},
			component: () => import('@/view/applatsEdition/message.vue')
		  },
		  {
			path: 'queryEdition',
			name: 'queryEdition',
			meta: {
			  hideInMenu: true,
			  icon: 'md-flower',
			  title: route => `版本 - ${route.query.name}`,
			  notCache: true
			},
			component: () => import('@/view/applatsEdition/message_edit.vue')
		  },
		  {
			path: 'article',
			name: '文章管理',
			meta: {
			  icon: '',
			  title: '文章管理'
			},
			component: () => import('@/view/applatsArticle/article.vue')
		  },
		  {
			path: 'queryArticle',
			name: 'queryArticle',
			meta: {
			  hideInMenu: true,
			  icon: 'md-flower',
			  title: route => `${route.query.name} - ${route.query.id}`,
			  notCache: true
			},
			component: () => import('@/view/applatsArticle/article_message.vue')
		  },
		  {
			path: '/applatsKeyword',
			name: '关键词管理',
			meta: {
			  icon: '',
			  title: '关键词管理'
			},
			component: () => import('@/view/applatsKeyword/message.vue')
		  },

		  */
		]
	},
  {
    path: '/gam',
    name: 'gam',
    component: Main,
    meta: {
      icon: 'md-game-controller-b',
      title: '小游戏'
    },
    children: [
      {
        path: '/snake',
        name: 'snake',
        meta: {
          icon: '',
          title: '贪食蛇'
        },
        component: () => import('@/view/gam/snake.vue')
      },
    ]
  },
	/*	{
		path: '/Operate',
		name: '运营中心',
		component: Main,
		meta: {
		  icon: 'logo-snapchat',
		  title: '运营中心'
		},
		children: [
		  {
			path: 'activity',
			name: '活动管理',
			meta: {
			  icon: '',
			  title: '活动管理',
			  refresh: false,
			},
			component: () => import('@/view/operate/activity.vue')
		  },
		]
	  },*/
	/*{
	  path: '/Custom',
	  name: '自定义中心',
	  component: Main,
	  meta: {
	  icon: 'md-cut',
	  title: '自定义中心'
	  },
	  children: [
	  {
		path: 'customSetting',
		name: '自定义页管理',
		meta: {
		icon: '',
		title: '自定义页管理',
		refresh: false,
		},
		component: () => import('@/view/customSetting/list.vue')
	  },
	  ]
	},*/
	{
		path: '/platform',
		name: '平台配置',
		component: Main,
		meta: {
			icon: 'logo-codepen',
			title: '平台配置'
		},
		children: [
			{
				path: '/platformBanner',
				name: 'platformBanner',
				meta: {
					icon: '',
					title: 'Banner管理'
				},
				component: () => import('@/view/platformBanner/platformBannerList.vue')
			},
			{
				path: '/applatsFaq',
				name: 'applatsFaq',
				meta: {
					icon: '',
					title: 'FAQ管理'
				},
				component: () => import('@/view/applatsFaq/FaqList.vue')
			},
			{
				path: '/box-hot',
				name: 'boxHost',
				meta: {
					icon: '',
					title: '热门意见箱'
				},
				component: () => import('@/view/box-hot/box_hot.vue')
			},
			{
				path: 'testUser',
				name: 'testUser',
				meta: {
					icon: '',
					title: '测试用户'
				},
				component: () => import('@/view/testUser/testUser.vue')
			},
			{
				path: 'articleGuide',
				name: 'articleGuide',
				meta: {
					icon: '',
					title: '操作指引'
				},
				component: () => import('@/view/applatsArticle/article.vue')
			},
			{
				path: 'queryArticle',
				name: 'queryArticle',
				meta: {
					hideInMenu: true,
					icon: 'md-flower',
					title: route => `${route.query.name} - ${route.query.id}`,
					notCache: true
				},
				component: () => import('@/view/applatsArticle/article_message.vue')
			},
			// {
			//   path: '/NewsList',
			//   name: 'NewsList管理',
			//   meta: {
			//     icon: 'md-arrow-dropright',
			//     title: '内容资讯管理'
			//   },
			//   component: () => import('@/view/news/newsList.vue')
			// },
			// {
			//   path: 'newsEdition',
			//   name: 'newsEdition',
			//   meta: {
			//     hideInMenu: true,
			//     icon: 'md-flower',
			//     title: route => `内容资讯编辑 - ${route.query.id}`,
			//     notCache: true
			//   },
			//   component: () => import('@/view/news/newsEdition.vue')
			// },
		]
	},
	{
		path: '/Template',
		name: '消息中心',
		component: Main,
		meta: {
			icon: 'md-barcode',
			title: '消息中心'
		},
		children: [
			{
				path: 'message',
				name: 'templateList',
				meta: {
					icon: '',
					title: '消息模板',
					refresh: false,
				},
				component: () => import('@/view/message/template.vue')
			},
			{
				path: 'queryTemplate',
				name: 'queryTemplate',
				meta: {
					hideInMenu: true,
					icon: 'md-flower',
					title: route => `模板详情 - ${route.query.id}`,
					notCache: true
				},
				component: () => import('@/view/message/template_query.vue')
			},
			{
				path: 'messageLog',
				name: 'messageLog',
				meta: {
					icon: '',
					title: '消息日志',
					refresh: false,
				},
				component: () => import('@/view/message/template_log.vue')
			},
		]
	},
	{
		path: '/administration',
		name: '系统管理',
		component: Main,
		meta: {
			icon: 'md-cog',
			title: '系统管理'
		},
		children: [
			{
				path: 'admin',
				name: 'admin',
				meta: {
					icon: '',
					title: '管理员列表',
					refresh: false,
				},
				component: () => import('@/view/Administration/admin.vue')
			},
			{
				path: 'role',
				name: 'role',
				meta: {
					icon: '',
					title: '角色列表'
				},
				component: () => import('@/view/Administration/role.vue')
			},
			{
				path: 'adminMenu',
				name: 'adminMenu',
				meta: {
					icon: 't',
					title: '菜单列表',
					refresh: false,
				},
				component: () => import('@/view/Administration/menu.vue')
			},
			{
				path: 'adminLogin',
				name: 'adminLogin',
				meta: {
					icon: 't',
					title: '系统日志',
					refresh: false,
				},
				component: () => import('@/view/Administration/adminLog.vue')
			},
		]
	},

	{
		path: '/tradingCenter',
		name: 'tradingCenter',
		component: Main,
		meta: {
			icon: 'logo-yen',
			title: '交易中心'
		},
		children: [
			{
				path: 'withdrawalReview',
				name: 'withdrawalReview',
				meta: {
					icon: '',
					title: '提现审核',
					refresh: false,
				},
				component: () => import('@/view/tradingCenter/withdrawal_review.vue')
			},
			{
				path: 'queryWithdrawalReview',
				name: 'queryWithdrawalReview',
				meta: {
					hideInMenu: true,
					icon: 'md-flower',
					title: route => `提现详情 - ${route.query.id}`,
					notCache: true
				},
				component: () => import('@/view/tradingCenter/withdrawal_message.vue')
			},
			{
				path: 'rewardOrder',
				name: 'rewardOrder',
				meta: {
					icon: '',
					title: '打赏订单',
					refresh: false,
				},
				component: () => import('@/view/tradingCenter/reward_order.vue')
			},
			{
				path: 'queryRewardOrder',
				name: 'queryRewardOrder',
				meta: {
					hideInMenu: true,
					icon: 'md-flower',
					title: route => `订单详情 - ${route.query.id}`,
					notCache: true
				},
				component: () => import('@/view/tradingCenter/reward_order_message.vue')
			},
			{
				path: 'moneyDataCenter',
				name: 'moneyDataCenter',
				meta: {
					icon: '',
					title: '数据统计',
					refresh: false,
				},
				component: () => import('@/view/tradingCenter/data_center.vue')
			},
		]

	},


]

