import HttpRequest from '@/libs/axios'
import Cookies from 'js-cookie';
import config from '@/config'

//var tokenCheck='?access-token='+Cookies.get('accessToken');

const docPath = window.document.domain;

let gotoUrl;
if(docPath=='admin.uwants.fun'){
	gotoUrl = config.baseUrl.pro;
}else if(docPath=='admin.uwants.fun'){
	gotoUrl = config.baseUrl.proInx;
}else{
	gotoUrl = config.baseUrl.dev;
}



const baseUrl = process.env.NODE_ENV === 'development' ? config.baseUrl.dev : gotoUrl




const axios = new HttpRequest(baseUrl)
export default axios
