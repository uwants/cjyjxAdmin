export default {
  /**
   * @description 配置显示在浏览器标签的title
   */
  title: '管理后台',
  /**
   * @description token在Cookie中存储的天数，默认1天
   */
  cookieExpires: 0.5,
  /**
   * @description 是否使用国际化，默认为false
   *              如果不使用，则需要在路由中给需要在菜单中展示的路由设置meta: {title: 'xxx'}
   *              用来在菜单中显示文字
   */
  useI18n: false,
  /**
   * @description api请求基础路径
   */
  baseUrl: {
    //开发环境
    dev: 'http://www.miying.com/admin',
    //生产环境
    //pro: 'http://hr-20190515jyjs:288/admin',
    //测试环境
    pro: 'http://uat.uwants.fun/admin',
    //正式环境
    proInx: 'http://uat.uwants.fun/admin',
  },


//图片上传地址(本地)
  Image_url_dev: 'http://uat.uwants.fun/admin/uploadImage',

//生产环境
//Image_url_pro:'http://hr-20190515jyjs:288/admin/uploadImage',

//测试环境
  Image_url_pro: 'http://uat.uwants.fun/admin/uploadImage',


  Image_url: process.env.NODE_ENV === 'development' ?
    'http://uat.uwants.fun/api/image/create' :
    'http://uat.uwants.fun/api/image/create',


  HeaderImage: process.env.NODE_ENV === 'development' ?
    'http://uat.uwants.fun/storage/myself/header.png' :
    'http://uat.uwants.fun/storage/myself/header.png',

  BackgroundImage: process.env.NODE_ENV === 'development' ?
    'http://uat.uwants.fun/storage/myself/background.png' :
    'http://uat.uwants.fun/storage/myself/background.png',
  /**
   * @description 默认打开的首页的路由name值，默认为home
   */
  homeName: 'home',
  /**
   * @description 需要加载的插件
   */
  plugin: {
    'error-store': {
      showInHeader: false, // 设为false后不会在顶部显示错误日志徽标
      developmentOff: true // 设为true后在开发环境不会收集错误信息，方便开发中排查错误
    }
  },

  //分页大小配置
  PAGE_SIZE_OPTS: [10, 20, 50, 100],
  PAGE_SIZE_DEF: 10,


}
